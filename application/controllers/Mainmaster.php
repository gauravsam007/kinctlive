<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmaster extends MY_Controller {

	public function index()
	{
		$req_url	= $_SERVER['REQUEST_URI'];
		$this->appURL($req_url);
		
		if($this->viewload == "index"){			// Index
			echo "index";
			$this->load->view('welcome_message');
		}elseif($this->viewload == "login"){
			$this->load->model('login_model');
			$this->login_model->getuser();
			$this->load->view('welcome_message');
		}elseif($this->viewload == "register"){
			echo "register";
			$this->load->view('welcome_message');
		}elseif($this->viewload == "change-password"){
			echo "change password";
			$this->load->view('welcome_message');
		}elseif($this->viewload == "profile"){
			echo "profile";
			$this->load->view('welcome_message');
		}elseif($this->viewload == "add-profile"){
			echo "add profile";
			$this->load->view('welcome_message');
		}
		elseif($this->viewload == "service"){
			echo "service";
			echo SERVICEID;
			$this->load->view('welcome_message');
		}else{
			echo "other";
		}
		
		
		
	}
	
}
