<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	
	/*Register New User*/
	function signup($salt)
	{
		//$salt = self::generateSalt(3);
		$password = $this->input->post('password');
		$passw_hash = md5(md5($password).$salt); 
		$signup_data = array(
				'emailid' 		=> $this->input->post('emailid'),
				'country_code' 	=> $this->input->post('country_code'),
				'phone_number' 	=> $this->input->post('phone_number'),			
				'facebook_id' 	=> $this->input->post('facebook_id'),
				'password' 		=> $passw_hash,
				'otp' 			=> $this->input->post('otp'),
				'otp_gen_time' 	=> date("Y-m-d h:i:s"),
				'createddate' 	=> $this->input->post('createddate')
			);
			if($this->db->insert('user', $signup_data))
			{
			$last_id = $this->db->insert_id();
			
			
		$basicdetail_data = array(
				'user_id' => $last_id,
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name')
			);
			
		$this->db->insert('userbasicdetail', $basicdetail_data);
		
	}
	else{
		echo "error in query1";
	}
	}
	/*end Register*/
	
}