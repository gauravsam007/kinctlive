<?php
/**
 * Session
 * @Created By	Kamran
 * @Created on 17th Jan 2017
 * @ This is Base Class
 * @ Base Controller
 */ 

class MY_Controller extends CI_Controller {
	public $viewload = "";
	
	// Get URLs
	public function appURL($req_url){
		
		$url_arr	= explode("/", $req_url);
		$num		= count($url_arr);
		
		$url_para	= explode("?", $url_arr[1]);
		
		$url		= explode("-", str_replace(".html", "", $url_para[0]));
		$num_url	= count($url);
		
		$id = str_replace("id", "", $url[$num_url-1]);
		
		//for serice
		$this->getServiceProvider($url_arr[1], $id);
		
		$url_id		= explode("_", $url[count($url)-1]);
		$num_row	= count($url_id);
		
		if(@$url_arr[1]=='' or @$url_arr[1]=='index.php' or @$url_para[0]=="index.php" || @$url_para[0]==""){
			$this->viewload = "index";
		}elseif(@$url_arr[1]=='login' or @$url_para[0]=="login"){
			$this->viewload = "login";
		}elseif(@$url_arr[1]=='register' or @$url_para[0]=="register"){
			$this->viewload = "register";
		}elseif(@$url_arr[1]=='change-password' or @$url_para[0]=="change-password"){
			$this->viewload = "change-password";
		}elseif(@$url_arr[1]=='profile' or @$url_para[0]=="profile"){
			$this->viewload = "profile";
		}elseif(@$url_arr[1]=='add-profile' or @$url_para[0]=="add-profile"){
			$this->viewload = "add-profile";
		}
		
		elseif(@$url[0]=='service' or @$url_para[0]=="service"){
			define('SERVICEID', $id);
			$this->viewload = "service";
		}
		
		
	}
	
	public function getServiceProvider($url_arr, $id){
		$this->load->model('getUrl');
		$this->getUrl->getServicesProvider($url_arr, $id);
	}

}
